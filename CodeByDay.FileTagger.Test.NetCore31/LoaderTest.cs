﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodeByDay.FileTagger.Test.NetCore31
{
    public class LoaderTest
    {
        private String FILE_ROOT => TestContext.CurrentContext.TestDirectory;

        [Test]
        public void Trial()
        {
            var Loader = new ConstantTagger();

            var file = "CodeByDay.FileTagger.dll";
            var path = Loader.GetPath(file);

            Assert.True(file == path.Split('?')[0]);
            Assert.True(path.Split('=')[1].Length > 0);
        }

        private static String H = "{h}";

        public static IEnumerable<String> TaggerTypes
        {
            get
            {
                yield return nameof(HashTagger);
                yield return nameof(WriteTimeTagger);
                yield return nameof(ConstantTagger);
            }
        }

        public static IEnumerable<String> Files
        {
            get
            {
                yield return "dummy.dummy";
                yield return "dummy";
                yield return "root/dummy.dummy";
                yield return "root/dummy";
                yield return "root/path/dummy.dummy";
                yield return "root/path/dummy";
            }
        }

        public static IEnumerable<String> Prefixes
        {
            get
            {
                yield return "";
                yield return "/";
                yield return "~/";
            }
        }

        public static IEnumerable<Object[]> JoinedTestData(TaggingMethod method, Func<string, string> finalizer)
        {
            foreach (var prefix in Prefixes)
            {
                foreach (var file in Files)
                {
                    foreach (var tagType in TaggerTypes)
                    {
                        yield return new Object[] { tagType, method, $"{prefix}{file}", prefix + finalizer(file) };
                    }
                }
            }
        }

        public static IEnumerable<Object[]> QueryStringTestData
        {
            get
            {
                return JoinedTestData(TaggingMethod.QueryString, file => $"{file}?tag={H}");
            }
        }

        public static IEnumerable<Object[]> PathTestData
        {
            get
            {
                return JoinedTestData(TaggingMethod.Path, file =>
                {
                    var index = file.LastIndexOf('/');
                    return index > -1 ? file.Insert(index, "/tag=" + H) : "tag=" + H + "/" + file;
                });
            }
        }

        public static IEnumerable<Object[]> FileNameTestData
        {
            get
            {
                return JoinedTestData(TaggingMethod.FileName, file =>
                {
                    var index = file.LastIndexOf('.');
                    return index > -1 ? file.Insert(index, ".tag=" + H) : null;
                }).Where(a => ((String)a[2]).LastIndexOf('.') > -1);
            }
        }

        public static IEnumerable<Object[]> HashesTestData => QueryStringTestData.Concat(PathTestData).Concat(FileNameTestData);
        [Test]
        [TestCaseSource(nameof(HashesTestData))]
        public void Hashes(String tagType, TaggingMethod method, String input, String output)
        {
            var Loader = Invoke(tagType, method);

            var hash = Loader.GetTag(input);
            var path = Loader.GetPath(input);

            Assert.AreEqual(output.Replace(H, hash), path);
        }

        public static IEnumerable<Object[]> RedirectsTestData => PathTestData.Concat(FileNameTestData).Select(o => o.Take(3).ToArray());
        [Theory]
        [TestCaseSource(nameof(RedirectsTestData))]
        public void Redirects(String tagType, TaggingMethod method, String input)
        {
            var Loader = Invoke(tagType, method);

            var pattern = method.GetRewriteRule();
            var match = Regex.Match(Loader.GetPath(input), pattern.Item1);

            if (match.Groups.Count < 4)
            {
                Assert.AreEqual("Not enough groups!", "");
            }

            Assert.AreEqual(input, pattern.Item2.Replace("$1", match.Groups[1].Captures[0].Value).Replace("$3", match.Groups[3].Captures[0].Value));
        }

        private Tagger Invoke(String tagType, TaggingMethod method)
        {
            switch (tagType)
            {
                case nameof(HashTagger):
                    return new HashTagger
                    {
                        FileRoot = FILE_ROOT,
                        TaggingMethod = method
                    };

                case nameof(WriteTimeTagger):
                    return new WriteTimeTagger
                    {
                        FileRoot = FILE_ROOT,
                        TaggingMethod = method
                    };

                default:
                case nameof(ConstantTagger):
                    return new ConstantTagger
                    {
                        TaggingMethod = method
                    };
            }
        }
    }
}
