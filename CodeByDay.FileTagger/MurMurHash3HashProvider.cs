﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeByDay.FileTagger
{
    internal class MurMurHash3HashProvider : IHashProvider
    {
        public string Tag(IEnumerable<byte> bytes)
        {
            using (var memStream = new MemoryStream(bytes.ToArray()))
            {
                return MurMurHash3.Hash(memStream).ToString("X");
            }
        }
    }
}
