﻿using System;
using System.Threading.Tasks;

namespace CodeByDay.FileTagger
{
    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching. Tags are a constant.
    /// </summary>
    public class ConstantTagger : Tagger, IConstantTagger
    {
        /// <summary>
        /// The constant used. Defaults to "constant".
        /// </summary>
        public String Constant { get; set; } = "constant";

        internal override string GetTag(string path)
        {
            return Constant;
        }

        internal override Task<string> GetTagAsync(string path)
        {
            return Task.FromResult(GetTag(path));
        }
    }
}
