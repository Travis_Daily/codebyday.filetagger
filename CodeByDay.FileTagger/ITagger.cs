﻿using System;
using System.Threading.Tasks;

namespace CodeByDay.FileTagger
{
    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching.
    /// </summary>
    public interface ITagger
    {
        /// <summary>
        /// The Tagging Method used.
        /// </summary>
        TaggingMethod TaggingMethod { get; set; }

        /// <summary>
        /// Returns the given path with a file tag appended as defined by the TaggingMethod.
        /// </summary>
        /// <param name="path">The path to the file as rendered in the HTML page.</param>
        string GetPath(string path);

        /// <summary>
        /// Returns the given path with a file tag appended as defined by the TaggingMethod.
        /// </summary>
        /// <param name="path">The path to the file as rendered in the HTML page.</param>
        Task<string> GetPathAsync(string path)
        {
            return Task.FromResult(GetPath(path));
        }
    }

    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching.
    /// </summary>
    public interface IFileTagger : ITagger
    {
        /// <summary>
        /// The length of time to store cached values.
        /// </summary>
        TimeSpan CacheTime { get; set; }

        /// <summary>
        /// A prefix to append to each fuctions given 'path' param to find the true location of the file.
        /// </summary>
        string FileRoot { get; set; }
    }

    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching.
    /// </summary>
    public interface IHashTagger : IFileTagger
    {
        /// <summary>
        /// The IHashProvider used to hash the files.
        /// </summary>
        IHashProvider HashProvider { get; set; }
    }

    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching.
    /// </summary>
    public interface IConstantTagger : ITagger
    {
        /// <summary>
        /// The constant used.
        /// </summary>
        String Constant { get; set; }
    }
}
