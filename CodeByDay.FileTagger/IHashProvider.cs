﻿using System.Collections.Generic;

namespace CodeByDay.FileTagger
{
    /// <summary>
    /// Provides hashing fuctions.
    /// </summary>
    public interface IHashProvider
    {
        /// <summary>
        /// Converts the given bytes into a hash.
        /// </summary>
        /// <param name="bytes">The bytes to hash.</param>
        /// <returns>A string hash.</returns>
        string Tag(IEnumerable<byte> bytes);
    }
}