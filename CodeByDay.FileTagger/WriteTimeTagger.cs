﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace CodeByDay.FileTagger
{
    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching. Tags are the file's last write time.
    /// </summary>
    public class WriteTimeTagger : FileTagger, IFileTagger
    {
        internal override string GetTag(string path)
        {
            var fileName = GetFileName(path);
            return tagCache.Get(fileName, () =>
            {
                var lastWriteTime = File.GetLastWriteTimeUtc(fileName);
                // When the file can't be found, a date in the year 1601 is returned.
                if (lastWriteTime.Year == 1601)
                {
                    throw new Exception($"File at the path '{Path.GetFullPath(fileName)}' was last modified in year 1601: '{lastWriteTime}'. Either the metadata is wrong, or the file cannot be found.");
                }
                return lastWriteTime.ToString("yyMMddhhmmss");
            });
        }

        internal override Task<string> GetTagAsync(string path)
        {
            return Task.FromResult(GetTag(path));
        }
    }
}
