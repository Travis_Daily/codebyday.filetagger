﻿using System;
using System.Threading.Tasks;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("CodeByDay.FileTagger.Test.NetCore31")]

namespace CodeByDay.FileTagger
{
    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching.
    /// </summary>
    public abstract class Tagger : ITagger
    {
        internal const String TAG_PHRASE = "tag=";

        /// <summary>
        /// The Tagging Method used. Defaults to QueryString.
        /// </summary>
        public TaggingMethod TaggingMethod { get; set; } = TaggingMethod.QueryString;

        /// <inheritdoc />
        public string GetPath(string path)
        {
            return InsertTag(path, $"{TAG_PHRASE}{GetTag(path)}");
        }

        /// <inheritdoc />
        public async Task<string> GetPathAsync(string path)
        {
            return InsertTag(path, $"{TAG_PHRASE}{await GetTagAsync(path)}");
        }

        private string InsertTag(string path, string fullTag)
        {
            switch (TaggingMethod)
            {
                default:
                case TaggingMethod.QueryString:
                    return $"{path}?{fullTag}";

                case TaggingMethod.Path:
                    if (path.Contains("/"))
                    {
                        return path.Insert(path.LastIndexOf("/"), $"/{fullTag}");
                    }
                    else
                    {
                        return $"{fullTag}/{path}";
                    }

                case TaggingMethod.FileName:
                    if (path.Contains("."))
                    {
                        return path.Insert(path.LastIndexOf("."), $".{fullTag}");
                    }
                    else
                    {
                        return $"{path}.{fullTag}";
                    }
            }
        }

        internal abstract string GetTag(string path);
        internal abstract Task<string> GetTagAsync(string path);
    }
}
