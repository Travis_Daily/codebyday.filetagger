﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Rewrite;
using System;

namespace CodeByDay.FileTagger
{
    /// <summary>
    /// The method used to rewrite the filepath.
    /// </summary>
    public enum TaggingMethod
    {
        /// <summary>
        /// Appends a query param to the file name. path/file.css?tag=CCDED65A
        /// </summary>
        QueryString,

        /// <summary>
        /// Adds a fake folder in the filepath. path/tag=CCDED65A/file.css . You MUST use RewriteRules!
        /// </summary>
        Path,

        /// <summary>
        /// Adds a fake section in the file name. path/file.tag=CCDED65A.css . You MUST use RewriteRules! The result when given a file without an extension is undefined.
        /// </summary>
        FileName
    }

    /// <summary>
    /// Functions for TaggingMethods.
    /// </summary>
    public static class TaggingMethodExtensions
    {
        private static readonly String PATH_REWRITE = $"(.*?)({Tagger.TAG_PHRASE}[^/]*?)/([^/]*)$";
        private static readonly String FILENAME_REWRITE = $"(.*?)\\.({Tagger.TAG_PHRASE}[^\\.]*?)\\.([^\\.]*)$";
        private const String PATH_URL = "$1$3";
        private const String FILENAME_URL = "$1.$3";

        /// <summary>
        /// The regex and the replacement needed for given TaggingMethod. Build a rule thusly: new RewriteOptions().AddRewrite(taggingMethod.GetRewriteRule().Item1, taggingMethod.GetRewriteRule().Item2, true);
        /// </summary>
        /// <param name="taggingMethod">The TaggingMethod expected for the app.</param>
        /// <returns>Tuple(regex, replacement)</returns>
        public static Tuple<String, String> GetRewriteRule(this TaggingMethod taggingMethod)
        {
            switch (taggingMethod)
            {
                default:
                case TaggingMethod.QueryString:
                    return null;

                case TaggingMethod.Path:
                    return Tuple.Create(PATH_REWRITE, PATH_URL);

                case TaggingMethod.FileName:
                    return Tuple.Create(FILENAME_REWRITE, FILENAME_URL);
            }
        }

        /// <summary>
        /// Gets a RewriteOptions if one is needed for the given TaggingMethod.
        /// </summary>
        /// <param name="taggingMethod">The TaggingMethod expected for the app.</param>
        /// <returns>A RewriteOptions if one is needed, else null.</returns>
        public static RewriteOptions GetRewriteOptions(this TaggingMethod taggingMethod)
        {
            var rule = taggingMethod.GetRewriteRule();
            if (rule != null)
            {
                return new RewriteOptions().AddRewrite(rule.Item1, rule.Item2, true);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Sets a rewrite rule on the given app if one is needed.
        /// </summary>
        /// <param name="taggingMethod">The TaggingMethod expected for the app.</param>
        /// <param name="app">The app on which the rewrite is needed.</param>
        public static void SetRewriteRule(this TaggingMethod taggingMethod, IApplicationBuilder app)
        {
            var options = taggingMethod.GetRewriteOptions();
            if (options != null)
            {
                app.UseRewriter(options);
            }
        }
    }
}
