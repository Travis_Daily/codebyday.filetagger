﻿using System.IO;
using System.Threading.Tasks;

namespace CodeByDay.FileTagger
{
    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching. Tags are a hash based on the file contents.
    /// </summary>
    public class HashTagger : FileTagger, IHashTagger
    {
        private IHashProvider _hasher = new MurMurHash3HashProvider();
        /// <summary>
        /// The IHashProvider used to hash the files. Defaults to MurMurHash3.
        /// </summary>
        public IHashProvider HashProvider
        {
            get
            {
                return _hasher;
            }
            set
            {
                _hasher = value;
                tagCache.Clear();
            }
        }

        internal override string GetTag(string path)
        {
            var fileName = GetFileName(path);
            return tagCache.Get(fileName, () =>
            {
                var content = File.ReadAllBytes(fileName);
                return HashProvider.Tag(content);
            });
        }

        internal override Task<string> GetTagAsync(string path)
        {
            var fileName = GetFileName(path);
            return tagCache.GetAsync(fileName, async () =>
            {
                var content = await File.ReadAllBytesAsync(fileName);
                return HashProvider.Tag(content);
            });
        }
    }
}
