﻿using CodeByDay.Cache;
using System;
using System.IO;
using System.Linq;

namespace CodeByDay.FileTagger
{
    /// <summary>
    /// Loads HTML tags with a tag to circumvent Browser caching.
    /// </summary>
    public abstract class FileTagger : Tagger, IFileTagger
    {
        /// <summary>
        /// The length of time to store cached values. Defaults to TimeSpan.Zero
        /// </summary>
        public TimeSpan CacheTime { get { return tagCache.TimeToKeep; } set { tagCache.TimeToKeep = value; } }

        /// <summary>
        /// A prefix to append to each fuctions given 'path' param to find the true loaction of the file. Defaults to wwwroot/.
        /// </summary>
        public string FileRoot { get; set; } = "wwwroot/";

        /// <summary>
        /// protected internal
        /// </summary>
        protected internal String GetFileName(String path)
        {
            var triggers = new[] { "~", "/", "\\" };
            while (triggers.Any(t => path.StartsWith(t)))
            {
                path = path.Substring(1);
            }

            var fileRoot = FileRoot;
            while (path.StartsWith("../"))
            {
                path.Substring(3);
                fileRoot = new DirectoryInfo(fileRoot).Parent.FullName;
            }

            return Path.Combine(fileRoot, path);
        }

        /// <summary>
        /// The cache to store items in.
        /// </summary>
        protected static ITimedCache<string, string> tagCache = new TimedCache<string, string>() { TimeToKeep = TimeSpan.Zero };
    }
}
