# Code By Day: File Tagger #

Repo for the *Code By Day: File Tagger NuGet* package: [nuget.org/packages/CodeByDay.FileTagger](https://www.nuget.org/packages/CodeByDay.FileTagger)

Creates HTML paths with a tag to circumvent Browser caching.
Static files should be set to cache indefinitely. However, this creates an issue when you need to update said static files. CodeByDay.FileTagger offers the solution. This NuGet allows adding a tag to a static file. By updating this tag when the file is modified, browser caches are successfully circumvented.

# Release Notes #

##### 1.3.0 [2022 Mar 14] ([changes](https://bitbucket.org/Travis_Daily/codebyday.filetagger/branches/compare/1.3.0%0D1.2.0?w=1#diff))

* Removed support for .NET Framework and obsolete .NET Core targets. Added support for .NET Core 3.1 and .NET 5.

* Added GetPathAsync().

##### 1.2.0 [2020 Sep 21] ([changes](https://bitbucket.org/Travis_Daily/codebyday.filetagger/branches/compare/1.2.0%0D1.1.0?w=1#diff))

* Added explicit support for .NET 4.6.1 as urged by the [guidance](https://docs.microsoft.com/en-us/dotnet/standard/library-guidance/cross-platform-targeting).

* Removed support for .NET 3.5 in favor of .NET 4.5. Stay on 1.1.0 if you need to support 3.5 or 4.0.

#### 1.1.0 [2019 Apr 14] ([changes](https://bitbucket.org/Travis_Daily/codebyday.filetagger/branches/compare/1.1.0%0D1.0.1?w=1#diff))

* Added SetRewriteRule() and GetRewriteOptions() to be used instead of GetRewriteRule() in most cases.

* Added support for .NET Framework 3.5.

* Added .NET Standard 1.3 target.

#### 1.0.1 [2019 Mar 12] ([changes](https://bitbucket.org/Travis_Daily/codebyday.filetagger/branches/compare/1.0.1%0D1.0.0?w=1#diff))

* Now requiring Microsoft.AspNetCore.Rewrite in .NET Core 1.0 and .NET Standard 2.0 since that NuGet is needed to create the needed rewrites in the sample setup code.

# Usage Guide #

## File Tagger Kinds ##

1. ConstantTagger: Tags files with a given constant ("tag=constant"). Useful if you deploy with set versions.

2. WriteTimeTagger: Tags files with the destination file's write time as reported by the operating system ("tag=190102030405"). Useful if files are only written when they are updated.

3. HashTagger: Tags files with a hash of the file's content ("tag=CCDED65A"). This Tagger ensures that the user won't need to download the file again if it didn't change, but the write time or your deployed version did.

## Tagging Methods ##

1. QueryString: Appends a query param to the file name ("path/file.css?tag=CCDED65A"). This should be fine in most cases, but may potentially cause files to not be cached. Query params, after all, are suppose to be indicative of an action, not a static file.

2. Path: Adds a fake folder in the filepath ("path/tag=CCDED65A/file.css"). You MUST use RewriteRules! This looks more correct in the source code and should never fail to cache.

3. FileName: Adds a fake section in the file name ("path/file.tag=CCDED65A.css"). You MUST use RewriteRules! The result when given a file without an extension is undefined. This could potentially cause issues if browsers only expect a single dot, but should never fail to cache.

## Using a Tagger ##

Pass the native path into the GetPath function of a tagger to transform it with the tagger's method.

```
<link rel="stylesheet" type="text/css" href="@(new HashTagger().GetPath("/css/bundle.min.css"))" />

```

This yields the following in HTML:

```
<link rel="stylesheet" type="text/css" href="/css/bundle.min.css?tag=CCDED65A" />

```

I recommend using a singleton and accessing that same instance for all files. This allows you to only need to configure the Tagger in one place. It also allows the WriteTimeTagger and HashTagger to cache their results for a configurable about of time.

## Configuring a Tagger ##

Configuration can be done entirely in your Startup.Configure() function:

```
#!c#
            // In my implementation, I've set up a singleton, FileTagger.Current, which is an ITagger: public static class FileTagger { public static ITagger Current { get; set; } }
            // You can use a different File Tagger Kind: I use the HashTagger here as an example.
            FileTagger.Current = new HashTagger
            {
                TaggingMethod = TaggingMethod.Path, // Use whichever method you'd like.
                // Omit this compile-time if when using a Constant tagger. You should set the 'Constant' Property instead.
#if !DEBUG
                CacheTime = System.TimeSpan.FromHours(1) // Use whatever length of time you'd like. I'd recommend only setting this caching in production.
#endif
            };
    
            // Set rewrites if we need to. You can also use GetRewriteOptions() or GetRewriteRule() if needed.
            FileTagger.Current.TaggingMethod.SetRewriteRule(app);

```

## File Paths ##

For the WiteTimeTagger and HashTagger, the file must be readable. If the file is relative to the project root, nothing needs to be done. However, if you have a relative path, you need to set the FileRoot property. You can do this by appending the extra path sections.

Example: assume the currently loaded page is https://www.example.com/home/garden/soil/index.html

The following link will load https://www.example.com/home/garden/soil/soil.css

```
<link rel="stylesheet" type="text/css" href="soil.css" />

```

To make it work with the FileTagger, update the FileRoot:

```
@{
    var tagger = new HashTagger();
    tagger.FileRoot = Path.Combine(tagger.FileRoot, "home/garden/soil");
}
<link rel="stylesheet" type="text/css" href="@(tagger.GetPath("soil.css"))" />

```

Of course, it's much easier to use a full link. This feature is intended for rare use cases.

# Supported .NET Versions #

* .NET Core 3.1
* .NET 5

Additionally, this project targets [.NET Standard 2.0](https://docs.microsoft.com/en-us/dotnet/standard/net-standard#net-implementation-support). However, the actual implementation is undefined on this platform. These targets are only intended to allow the loading of this project if needed.

# Repo #

## Branches ##

The branches in this repo relate directly to the release they are building up to. Once a release has been made, the branch is no longer commited to. This effectively means there is no "master" branch: only releases.

## Versioning ##

This project uses [Semantic Versioning 2.0.0](https://semver.org)

If you're making a NuGet package, I recommend editing your .csproj to force this Semantic Versioning. This example requires a version equal to or above 1.3.0 and disallows 2.0.0 or above.

```
#!c#
<PackageReference Include="CodeByDay.FileTagger" Version="[1.3.0,2.0.0)" />
```

# Have an Issue? #

If you find any issues with this NuGet, please use the [Contact owners](https://www.nuget.org/packages/CodeByDay.FileTagger/1.3.0/ContactOwners) link on the NuGet page.